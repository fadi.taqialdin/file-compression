#!/bin/sh

cd /var/www
chmod -R 777 /var/www/storage
chmod -R 775 /var/www/bootstrap/cache
composer install --ignore-platform-reqs
#wait for db to be ready
sleep 30
php artisan migrate
