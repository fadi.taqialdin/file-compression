<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZanySoft\Zip\Zip;

class CompressFile
{
    public function handle(array $payload)
    {
        if(!isset($payload['id']) || !isset($payload['name'])  || !isset($payload['raw'])){
            return false;
        }

        $tmpName = $payload['name'];
        $zipName = Str::random();
        Storage::disk('local')->put($tmpName, base64_decode($payload['raw']));

        $zip = Zip::create(storage_path('app/'.$zipName));
        $zip->add(storage_path('app/'.$tmpName));
        $zip->close();

        Storage::disk('local')->delete($tmpName);
        publish('file.compressed', [
            'id' => $payload['id'],
            'compressed' => base64_encode(Storage::disk('local')->get($zipName))
        ]);
        Storage::disk('local')->delete($zipName);

        // Stopping The Propagation Of The Event
        return false;
    }
}
