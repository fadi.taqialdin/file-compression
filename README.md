## About

This microservice made to provide file compression functionality.
Following `Producer/Consumer` pattern, this microservice depends on RabbitMQ using Laravel's events and queues.

## Usage

This microservice deals with two queues:

1. As `consumer` of the queue `file.uploaded`: to consume the file needs to be compressed.
payload data should be as the following:
```json
{
    "raw": "raw file",
    "name": "name of the file", 
    "id": 1
}
```
`raw`: The file it self base64 encoded. <br/>
`name`: The file name to be persisted in ZIP. <br/>
`id`: This field will be returned (through `file.compressed` queue) as is when the compression done.

2. As `producer` to the queue `file.compressed`: to publish the file compressed ZIP.
payload data would be as the following:
```json
{
    "compressed": "compressed file",
    "id": 1
}
```
`compressed`: The file compressed ZIP base64 encoded. <br/>
`id`: Returned as is.

## How to run

1. Install the application dependencies using docker
```
docker-compose up
```
2. Please wait about a minute for composer installation and db migrations get ready.

3. Visit `http://127.0.0.1:8001`.

## Improvements

Right now we are using RabbitMQ as the transport layer to sending files from/to this service, 
however it is not efficient with a large load of files nor large files, 
so we can improve by sending URI of the file on a cloud storage that is accessible from the producer service and the consumer service 
(and that applies both ways for raw files and compressed files).   

## Alternative Solutions Comparison

There are different ways to interconnect microservices, such as:

- **Brokers (eg. RabbitMQ):**
  The use of Message Queues provides a way for parts of the application to push messages to a queue asynchronously and ensure they are delivered to the correct destination. 
  The message broker provides temporary message storage when the receiving service is busy or disconnected. 
  The messages stay in the queue until the consumer handles them and removes them.

- **Remote Procedure Calls (RPC) or REST APIS:**
  One of the major characteristics of these solutions is they are synchronous, consumers receive the response after a request is sent.
